#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0
test="/usr/share/pmix/test/pmix_test"

$test -h
let ret=$ret+$?

$test --test-publish
let ret=$ret+$?

$test --test-connect
let ret=$ret+$?

$test --test-resolve-peers
let ret=$ret+$?

$test --test-error
let ret=$ret+$?

$test --test-internal 10
let ret=$ret+$?

exit $ret
